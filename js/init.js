(function($){
  $(function(){

    $('.sidenav').sidenav();
    $('.parallax').parallax();

    $('.zemaitija').mouseover(function(){
      // pakuriu mouseover eventa, kuris pakeis foto uzvedus pele
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapisZemaitija.jpg');
    });
    $('.zemaitija').mouseleave(function(){
      // pakuriu mouseleave eventa, kuris pakeis foto nuvedus pele
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapis.jpg');
    });
        $('.aukstaitija').mouseover(function(){
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapisAukstaitija.jpg');
    });
    $('.aukstaitija').mouseleave(function(){
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapis.jpg');
    });
        $('.suvalkija').mouseover(function(){
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapisSuvalkija.jpg');
    });
    $('.suvalkija').mouseleave(function(){
      $('#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapis.jpg');
    });
        $('.dzukija').mouseover(function(){
      $('img#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapisDzukija.jpg');
    });
    $('.dzukija').mouseleave(function(){
      $('img#zemelapio-nuotrauka').attr('src', 'Nuotraukos/LietuvosZemelapis.jpg');
    });



  }); // end of document ready
})(jQuery); // end of jQuery name space

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.collapsible');
    var options = {
    	outDuration: 2000
    };
    var instances = M.Collapsible.init(elems, options);

  });
  $(".dropdown-trigger").dropdown();

  document.addEventListener('DOMContentLoaded', function() {
    var elems = document.querySelectorAll('.carousel');
    var instances = M.Carousel.init(elems, options);
  });

  // Or with jQuery

  $(document).ready(function(){
    $('.carousel').carousel();
  });